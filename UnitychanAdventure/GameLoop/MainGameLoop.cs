﻿using System;
using System.Collections.Generic;
using System.Drawing;
using NLua;
using OpenTK;
using OpenTK.Input;
using Tri;
using Tri.Mathematics;
using Tri.SystemComponent;
using Tri.Win.SystemComponent;
using Tri.Win;
using Tri.Win.GameLoop;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Camera;
using Tri.Win.Sound;
using Tri.Win.UI;
using UnitychanAdventure.GameObject;
using UnitychanAdventure.Map;
using UnitychanAdventure.Physics;
using UnitychanAdventure.Serif;

namespace UnitychanAdventure.GameLoop
{
    public class MainGameLoop : GameLoopBase
    {
        public static MainGameLoop Instance;
        public MapData MapData;
        public SerifManager SerifManager;

        Player pl;
        OrthographicCamera ocam;
        PhysicsHelper phelper;

        Sprite background;

        static SoundContext bgm;
        static Texture backgroundTexture;

        bool gameCleared;

        public MainGameLoop()
            : base("_MainGameLoop")
        {
        }

        public static async void LoadResource()
        {
            // bgm = await SoundContext.CreateAsync("Resources/Sound/Background1.wav");
            backgroundTexture = await Texture.CreateFromBitmapPathAsync("Resources/Image/Background.png");
        }

        protected override void OnInitialize()
        {
            Instance = this;

            background = new Sprite()
            {
                Texture = backgroundTexture,
                Size = GameSystem.MainForm.Viewport,
                Anchor = Vector2.Zero,
                Color = Color.FromArgb(60, 255, 255, 255),
            };
            // bgm.SetPlayback(true);

            MapData = MapManager.GetCurrentMap();
            MapData.InitGameObjects();
            MapData.Reset();
            phelper = new PhysicsHelper(MapData);
            SerifManager = new SerifManager();

            SerifManager.RenderSerif(MapData.SerifList);

            pl = new Player(new Vector2(MapData.PlayerPos.X + 0.5f, MapData.PlayerPos.Y) * MapData.UnitLength);
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);

            // bgm.Play();
        }
        public override void Update(Second deltaTime)
        {
            // temporary code
            // can cause undefined behavoiur at low fps
            if (deltaTime > (Second)0.1f)
                return;

            bool updatable = true;
            if (MapData.UpdateAction != null)
                updatable = MapData.UpdateAction();
            
            #region player coordinate compensation
            if (phelper.IsPlayerOnGround(pl) == true)
                pl.OnGround(MapData.UnitLength * (1 + Mathf.Floor(pl.Position.Y / MapData.UnitLength)) - 0.01f);
            else
                pl.OnAir();

            float align = 45f;
            if (phelper.IsPlayerHeadBlocked(pl, align) == true)
                pl.OnHeadBlocked(Mathf.Floor((pl.Position.Y + align) / MapData.UnitLength) * MapData.UnitLength - align);
            if (phelper.IsPlayerRightSideBlocked(pl) == true)
                pl.OnSideBlocked(MapData.UnitLength * (Mathf.Floor(pl.Position.X / MapData.UnitLength + 0.5f) - 0.5f) - 0.01f);
            if (phelper.IsPlayerLeftSideBlocked(pl) == true)
                pl.OnSideBlocked(MapData.UnitLength * (Mathf.Floor(pl.Position.X / MapData.UnitLength - 0.5f) + 1.5f) + 0.01f);
            #endregion
            if (updatable == true && gameCleared == false)
            {
                if (SerifManager.Focused == false)
                {
                    #region RESET!
                    if (InputHelper.IsPressed(Key.R))
                    {
                        pl = new Player(new Vector2(MapData.PlayerPos.X + 0.5f, MapData.PlayerPos.Y) * MapData.UnitLength);
                        MapData.Reset();
                    }
                    #endregion
                    #region player-object interation
                    int x = Mathf.Floor(pl.Position.X / MapData.UnitLength);
                    int y = Mathf.Floor(pl.Position.Y / MapData.UnitLength + 0.02f);

                    if (InputHelper.IsPressed(Key.Z))
                    {
                        if (0 <= x && x < MapData.Width)
                        if (0 <= y && y < MapData.Height)
                            switch (MapData[x, y])
                            {
                                case TileType.LaserHorizontalNeg:
                                case TileType.LaserHorizontalPos:
                                case TileType.LaserVerticalNeg:
                                case TileType.LaserVerticalPos:
                                case TileType.LaserHorizontalNegRev:
                                case TileType.LaserHorizontalPosRev:
                                case TileType.LaserVerticalNegRev:
                                case TileType.LaserVerticalPosRev:
                                    {
                                        LaserEmitter le = MapData.GameObjectDic[Mathf.Godel(x, y)] as LaserEmitter;
                                        if (le.Controllable == true)
                                            le.Enabled ^= true;
                                    }
                                    break;

                                case TileType.PistonHorizontalNeg:
                                case TileType.PistonHorizontalPos:
                                case TileType.PistonHorizontalNegRev:
                                case TileType.PistonHorizontalPosRev:
                                case TileType.PistonVerticalNeg:
                                case TileType.PistonVerticalNegRev:
                                case TileType.PistonVerticalPos:
                                case TileType.PistonVerticalPosRev:
                                    {
                                        Piston pis = MapData.GameObjectDic[Mathf.Godel(x, y)] as Piston;
                                        if (pis.Controllable == true)
                                            pis.Enabled ^= true;    
                                    }
                                    break;

                                default:
                                    break;
                            }
                    }
                    #endregion
                    #region Goal
                    if (0 <= x && x < MapData.Width)
                    if (0 <= y && y < MapData.Height)
                    if (MapData[x, y] == TileType.Goal)
                    {
                        GameSystem.LogSystem.WriteLine("Player win!");
                        if (MapManager.GetCurrentMap() != MapManager.LastMap)
                        {
                            MapManager.MoveToNextMap();
                            MapData = MapManager.GetCurrentMap();
                            MapData.InitGameObjects();
                            MapData.Reset();

                            pl = new Player(new Vector2(MapData.PlayerPos.X + 0.5f, MapData.PlayerPos.Y) * MapData.UnitLength);
                            phelper = new PhysicsHelper(MapData);
                            SerifManager.RenderSerif(MapData.SerifList);
                        }
                        else
                        {
                            gameCleared = true;
                            SerifManager.RenderSerif(new List<SerifData>()
                            {
                                new SerifData(SerifData.unityChanSmileTexture, "드디어 집에 도착했어! 고마워!"),
                                new SerifData(SerifData.unityChanHappyTexture, "다음에 또 봐~"),
                                new SerifData(null, "Press Z to return to title ..."),
                            });
                        }
                    }
                    #endregion
                }

                #region Cheat
                #if DEBUG
                if (InputHelper.IsPressed(Key.Q))
                {
                    GameSystem.LogSystem.WriteLine("Player win w/ Cheat!");
                    if (MapManager.GetCurrentMap() != MapManager.LastMap)
                    {
                        MapManager.MoveToNextMap();
                        MapData = MapManager.GetCurrentMap();
                        MapData.InitGameObjects();
                        MapData.Reset();

                        pl = new Player(new Vector2(MapData.PlayerPos.X + 0.5f, MapData.PlayerPos.Y) * MapData.UnitLength);
                        phelper = new PhysicsHelper(MapData);
                        SerifManager.RenderSerif(MapData.SerifList);
                    }
                    else
                    {
                        gameCleared = true;
                        SerifManager.RenderSerif(new List<SerifData>()
                        {
                            new SerifData(SerifData.unityChanSmileTexture, "드디어 집에 도착했어! 고마워!"),
                            new SerifData(SerifData.unityChanHappyTexture, "다음에 또 봐~"),
                            new SerifData(null, "             Press Z to return to title ..."),
                        });
                    }
                }
                #endif
                #endregion
            }

            // temp code. need refactoring
            if (gameCleared == true)
            if (SerifManager.Focused == false)
            if (InputHelper.IsDown(Key.Z) == true)
            {
                // bgm.Stop();
                GameSystem.RunGameLoop(new TitleGameLoop());
            }

            pl.Update(deltaTime);
            MapData.Update(deltaTime);
            SerifManager.Update(deltaTime);
        }
        public override void Render(Second deltaTime)
        {
            background.Render(ref ocam.Matrix);

            if (MapData.PreRenderAction != null)
                MapData.PreRenderAction();
            MapData.Render(ref ocam.Matrix);
            pl.Render(ref ocam.Matrix);

            SerifManager.Render(ref ocam.Matrix);

            if (MapData.ProRenderAction != null)
                MapData.ProRenderAction();
        }
        public override void Dispose()
        {
            background.Dispose();
        }
        protected override void OnStop()
        {
        }
    }
}
