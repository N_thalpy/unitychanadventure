﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using Tri;
using Tri.Mathematics;
using Tri.SystemComponent;
using Tri.Win;
using Tri.Win.SystemComponent;
using Tri.Win.GameLoop;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Camera;
using Tri.Win.Sound;
using Tri.Win.UI;
using Tri.Win.UI.BitmapFont;

namespace UnitychanAdventure.GameLoop
{
    public class TitleGameLoop : GameLoopBase
    {
        enum SelectStatus
        {
            Start,
            Exit,
        }
        SelectStatus selectStatus;
        SoundContext bgm;

        OrthographicCamera ocam;

        Texture characterTex;
        Sprite characterSprite;
        AnimatedCurve<Vector2> characterPosCurve;

        BitmapFont bitmapFont;
        BitmapLabel label1;
        BitmapLabel label2;
        AnimatedCurve<Vector2> label1PosCurve;
        AnimatedCurve<Vector2> label2PosCurve;

        BitmapLabel startLabel;
        BitmapLabel exitLabel;
        AnimatedCurve<float> labelAlphaCurve;
        
        Second timer;

        public TitleGameLoop()
            : base("_TitleScreen")
        {
        }

        protected override void OnInitialize()
        {
            selectStatus = SelectStatus.Start;
            // bgm = SoundContext.Create("Resources/Sound/Background0.wav");
            // bgm.SetPlayback(true);

            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport * 0.5f);
            timer = Second.Zero;

            #region character
            characterTex = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_01.png");
            characterPosCurve = new AnimatedCurve<Vector2>(InterpolationType.Quadratic);
            characterPosCurve.AddKeyFrame(Second.Zero, new Vector2(1500, 400));
            characterPosCurve.AddKeyFrame((Second)0.7f, new Vector2(1000, 400));
            characterSprite = new Sprite()
            {
                Texture = characterTex,
                Size = new Vector2(1, 3189f / 2524f) * 500,
                Position = characterPosCurve.GetValue(Second.Zero),
            };
            #endregion
            #region Title label
            bitmapFont = BitmapFont.Create("Resources/BitmapFont/arial.fnt");
            bitmapFont.FontSize = 7;
            float initx = -400;
            float lastx = 185;
            float y1 = 540;
            float y2 = 450;

            label1PosCurve = new AnimatedCurve<Vector2>(InterpolationType.Quadratic);
            label1PosCurve.AddKeyFrame(Second.Zero, new Vector2(initx, y1));
            label1PosCurve.AddKeyFrame((Second)0.9f, new Vector2(initx, y1));
            label1PosCurve.AddKeyFrame((Second)1.4f, new Vector2(lastx, y1));
            label1 = new BitmapLabel()
            {
                BitmapFont = bitmapFont,
                Text = "Unitychan",
                Color = Color.White,
                Anchor = new Vector2(0f, 0.5f),
                Position = label1PosCurve.GetValue(Second.Zero),
            };

            label2PosCurve = new AnimatedCurve<Vector2>(InterpolationType.Quadratic);
            label2PosCurve.AddKeyFrame(Second.Zero, new Vector2(initx, y2));
            label2PosCurve.AddKeyFrame((Second)0.9f, new Vector2(initx, y2));
            label2PosCurve.AddKeyFrame((Second)1.4f, new Vector2(lastx, y2));
            label2 = new BitmapLabel()
            {
                BitmapFont = bitmapFont,
                Text = "Adventure",
                Color = Color.White,
                Anchor = new Vector2(0f, 0.5f),
                Position = label1PosCurve.GetValue(Second.Zero),
            };
            #endregion

            bitmapFont = BitmapFont.Create("Resources/BitmapFont/arial.fnt");
            bitmapFont.FontSize = 12;
            labelAlphaCurve = new AnimatedCurve<float>(InterpolationType.Quadratic);
            labelAlphaCurve.AddKeyFrame(Second.Zero, 0);
            labelAlphaCurve.AddKeyFrame((Second)1.2f, 0);
            labelAlphaCurve.AddKeyFrame((Second)1.4f, 255);
            startLabel = new BitmapLabel()
            {
                BitmapFont = bitmapFont,
                Text = "START",
                Color = Color.Transparent,
                Anchor = new Vector2(0.5f, 0.5f),
                Position = new Vector2(380, 300),
            };
            exitLabel = new BitmapLabel()
            {
                BitmapFont = bitmapFont,
                Text = "EXIT",
                Color = Color.Transparent,
                Anchor = new Vector2(0.5f, 0.5f),
                Position = new Vector2(380, 240),
            };

            // bgm.Play();
        }
        public override void Update(Second deltaTime)
        {
            timer += deltaTime;

            characterSprite.Position = characterPosCurve.GetValue(timer);
            label1.Position = label1PosCurve.GetValue(timer);
            label2.Position = label2PosCurve.GetValue(timer);
        
            Color white = Color.White;
            Color red = Color.Red;

            int alpha = (int)labelAlphaCurve.GetValue(timer);
            switch (selectStatus)
            {
                case SelectStatus.Start:
                    startLabel.Color = Color.FromArgb(alpha, red.R, red.G, red.B);
                    exitLabel.Color = Color.FromArgb(alpha, white.R, white.G, white.B);
                    break;

                case SelectStatus.Exit:
                    startLabel.Color = Color.FromArgb(alpha, white.R, white.G, white.B);
                    exitLabel.Color = Color.FromArgb(alpha, red.R, red.G, red.B);
                    break;
            }

            if (timer > (Second)1.4f)
            {
                if (InputHelper.IsPressed(Key.Up))
                    selectStatus = SelectStatus.Start;
                if (InputHelper.IsPressed(Key.Down))
                    selectStatus = SelectStatus.Exit;

                if (InputHelper.IsPressed(Key.Z))
                {
                    switch (selectStatus)
                    {
                        case SelectStatus.Start:
                            // bgm.Stop();
                            GameSystem.RunGameLoop(new MainGameLoop());
                            break;

                        case SelectStatus.Exit:
                            GameSystem.Exit();
                            break;
                    }
                }
            }
        }
        public override void Render(Second deltaTime)
        {
            Matrix4 mat = ocam.Matrix;

            characterSprite.Render(ref mat);
            label1.Render(ref mat);
            label2.Render(ref mat);
            startLabel.Render(ref mat);
            exitLabel.Render(ref mat);
        }

        protected override void OnStop()
        {
            // bgm.Stop();
        }
        public override void Dispose()
        {
            characterTex.Dispose();
            characterSprite.Dispose();
        }
    }
}

