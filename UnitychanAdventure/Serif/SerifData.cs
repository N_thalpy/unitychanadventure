﻿using System;
using Tri.Win.Rendering;

namespace UnitychanAdventure.Serif
{
    public class SerifData
    {
        public static Texture unityChanHappyTexture;
        public static Texture unityChanHappy2Texture;
        public static Texture unityChanAngryTexture;
        public static Texture unityChanSurpriseTexture;
        public static Texture unityChanSmileTexture;
        public static Texture unityChanHurtTexture;

        static SerifData()
        {
            unityChanHappyTexture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_01.png");
            unityChanHappy2Texture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_02.png");
            unityChanAngryTexture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_03.png");
            unityChanSurpriseTexture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_04.png");
            unityChanSmileTexture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_05.png");
            unityChanHurtTexture = Texture.CreateFromBitmapPath("Resources/Image/Portrait/portrait_kohaku_06.png");
        }

        public readonly Texture Texture;
        public readonly String Serif;

        public SerifData(Texture unityChanTexture, String serif)
        {
            Texture = unityChanTexture;
            this.Serif = serif;
        }
    }
}

