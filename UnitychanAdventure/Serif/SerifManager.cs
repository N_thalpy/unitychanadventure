﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using Tri;
using Tri.SystemComponent;
using Tri.Win;
using Tri.Win.Rendering;
using Tri.Win.SystemComponent;
using Tri.Win.UI;
using Tri.Win.UI.BitmapFont;

namespace UnitychanAdventure.Serif
{
    public class SerifManager
    {
        private List<SerifData> serifData;
        private int serifCount;

        private Sprite unityChanSprite;
        private BitmapLabel unityChanSerif;
        private Sprite serifBackground;

        private Second timer;
        private Second coolTime = (Second)0.05f;

        public bool Focused;

        static BitmapFont malgunGothic;

        public static async void LoadResource()
        {
            malgunGothic = await BitmapFont.CreateAsync("Resources/BitmapFont/malgungothic.fnt");
        }

        public SerifManager()
        {
            unityChanSprite = new Sprite()
            {
                Texture = null,
                Size = new Vector2(-1, 3189f / 2524f) * 450,
                Anchor = new Vector2(1, 0),
            };
            serifBackground = new Sprite()
            {
                Texture = Texture.CreateFromBitmapPath("Resources/Image/TextBox.png"),
                Position = new Vector2(GameSystem.MainForm.Viewport.X / 2, 150),
                Size = new Vector2(1, 62f / 252f) * 900,
            };
            unityChanSerif = new BitmapLabel()
            {
                Color = Color.White,
                BitmapFont = malgunGothic,
                Position = new Vector2(380, 230),
                Anchor = new Vector2(0, 1),
                Text = String.Empty,
            };

            unityChanSerif.BitmapFont.FontSize = 11;
        }

        public void RenderSerif(List<SerifData> data)
        {
            serifData = data;
            serifCount = 0;
            unityChanSerif.Text = String.Empty;

            if (data.Count != 0)
            {
                unityChanSprite.Texture = data[0].Texture;
                Focused = true;
            }
        }
        public void Update(Second deltaTime)
        {
            timer += deltaTime;
            if (serifCount < serifData.Count)
            {
                SerifData serif = serifData[serifCount];
                if (timer > coolTime && unityChanSerif.Text.Length < serif.Serif.Length)
                {
                    timer -= coolTime;
                    unityChanSerif.Text += serif.Serif[unityChanSerif.Text.Length];
                }

                if (InputHelper.IsPressed(Key.Z))
                {
                    if (unityChanSerif.Text.Length != serif.Serif.Length)
                        unityChanSerif.Text = serif.Serif;
                    else if (serifCount + 1 < serifData.Count)
                    {
                        serifCount++;
                        unityChanSerif.Text = String.Empty;
                        unityChanSprite.Texture = serifData[serifCount].Texture;
                    }
                    else
                        Focused = false;
                }
            }
        }
        public void Render(ref Matrix4 mat)
        {
            if (unityChanSprite.Texture == null)
                unityChanSprite.IsVisible = false;
            else
                unityChanSprite.IsVisible = true;

            if (Focused == true)
            {
                serifBackground.Render(ref mat);
                unityChanSprite.Render(ref mat);
                unityChanSerif.Render(ref mat);
            }
        }
    }
}

