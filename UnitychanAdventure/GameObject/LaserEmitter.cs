﻿using System;
using System.Collections.Generic;
using OpenTK;
using Tri;
using Tri.Win;
using Tri.Win.Rendering;
using Tri.Win.UI;
using Tri.Mathematics;
using UnitychanAdventure.GameLoop;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.GameObject
{
    public enum Direction
    {
        HorizontalPos,
        HorizontalNeg,
        VerticalPos,
        VerticalNeg,
    }
    public class LaserEmitter : GameObjectBase
    {
        //     ^ HPos
        // VNeg| VPos
        // <-- o -->
        //     |
        //     v HNeg
        private static Second coolTime = (Second)1.5f;

        private Direction direction;

        private static Texture onTexture;
        private static Texture offTexture;
        private static Texture laserTexture;

        private Sprite emitterSprite;

        private List<Sprite> laserSpriteList;
        private float unitLength;

        private Second timeRemain;

        private bool enabled;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (enabled == value)
                    return;

                enabled = value;
                timeRemain = coolTime;
            }
        }
        public bool Controllable
        {
            get
            {
                return timeRemain < Second.Zero;
            }
        }

        static LaserEmitter()
        {
            onTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/TempRaser_Red.png");
            offTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/TempRaser_Red_Off.png");
            laserTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/RedLight.png");
        }
        public LaserEmitter(Vector2 initPos, Direction direction, bool initEnabled)
            : base(initPos)
        {
            this.direction = direction;
            this.enabled = initEnabled;
            this.unitLength = MainGameLoop.Instance.MapData.UnitLength;
            laserSpriteList = new List<Sprite>();

            Degree angle = Degree.Zero;
            switch (direction)
            {
                case Direction.VerticalPos:
                    angle = (Degree)90;
                    break;

                case Direction.VerticalNeg:
                    angle = (Degree)270;
                    break;

                case Direction.HorizontalNeg:
                    angle = (Degree)180;
                    break;

                case Direction.HorizontalPos:
                    angle = (Degree)0;
                    break;

                default:
                    throw new NotImplementedException();
            }
            emitterSprite = new Sprite()
            {
                Texture = onTexture,
                Size = new Vector2(unitLength, unitLength),
                Position = Position,
                Angle = angle,
            };
        }

        public override void Move(Vector2 pos)
        {
            base.Move(pos);
            emitterSprite.Position = pos;
        }

        public override void Render(ref Matrix4 mat)
        {
            emitterSprite.Render(ref mat);
            foreach (Sprite sp in laserSpriteList)
                sp.Render(ref mat);
        }
        public override void Update(Second deltaTime)
        {
            timeRemain -= deltaTime;

            if (Controllable == true)
                emitterSprite.Texture = onTexture;
            else
                emitterSprite.Texture = offTexture;

            foreach (Sprite sp in laserSpriteList)
                sp.Dispose();
            laserSpriteList.Clear();

            if (Enabled == true)
            {
                Vector2 pos = new Vector2(Mathf.Floor(Position.X / unitLength), Mathf.Floor(Position.Y / unitLength));
                Vector2 dir = Vector2.Zero;
                Degree angle = Degree.Zero;
                switch (direction)
                {
                    case Direction.VerticalPos:
                        dir = new Vector2(0, 1);
                        angle = (Degree)90;
                        break;

                    case Direction.VerticalNeg:
                        dir = new Vector2(0, -1);
                        angle = (Degree)90;
                        break;

                    case Direction.HorizontalPos:
                        dir = new Vector2(1, 0);
                        break;

                    case Direction.HorizontalNeg:
                        dir = new Vector2(-1, 0);
                        break;

                    default:
                        throw new NotImplementedException();
                }

                pos += dir;
                MapData md = MainGameLoop.Instance.MapData;
                
                bool isCollided = false;
                while (true)
                {
                    if (pos.X < 0 || pos.Y < 0)
                        break;
                    if (pos.X >= md.Width || pos.Y >= md.Height)
                    {
                        do
                        {
                            laserSpriteList.Add(
                                new Sprite()
                                {
                                    Texture = laserTexture,
                                    Size = new Vector2(unitLength, unitLength),
                                    Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                    Angle = angle,
                                });
                            pos += dir;
                        } while (pos.X * unitLength < GameSystem.MainForm.Viewport.X &&
                                 pos.Y * unitLength < GameSystem.MainForm.Viewport.Y);
                        break;
                    }

                    switch (md[(int)pos.X, (int)pos.Y])
                    {
                        case TileType.Ground:
                            isCollided = true;
                            break;

                        case TileType.IceBlock:
                            {
                                IceBlock ice = md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)] as IceBlock;
                                ice.UnderHeat = true;

                                if (ice.Enabled == true)
                                    isCollided = true;
                                else
                                    isCollided = false;
                            }
                            break;

                        case TileType.LaserSensor1:
                        case TileType.LaserSensor2:
                        case TileType.LaserSensor3:
                            {
                                LaserSensor ls = md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)] as LaserSensor;
                                ls.Enabled = true;
                            }
                            break;

                        case TileType.Door1:
                        case TileType.Door1Rev:
                        case TileType.Door2:
                        case TileType.Door2Rev:
                        case TileType.Door3:
                        case TileType.Door3Rev:
                            {
                                Door d = md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)] as Door;
                                if (d.Enabled == false)
                                    isCollided = true;
                            }
                            break;

                        default:
                            break;
                    }
                    if (isCollided == true)
                        break;

                    laserSpriteList.Add(
                        new Sprite()
                        {
                            Texture = laserTexture,
                            Size = new Vector2(unitLength, unitLength),
                            Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                            Angle = angle,
                        });

                    pos += dir;
                }
            }
        }
    }
}

