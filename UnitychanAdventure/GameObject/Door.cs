﻿using System;
using OpenTK;
using Tri;
using Tri.Mathematics;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.Map;
using UnitychanAdventure.GameLoop;

namespace UnitychanAdventure.GameObject
{
    public class Door : GameObjectBase
    {
        #region static
        private static Texture doorOnTexture1;
        private static Texture doorOnTexture2;
        private static Texture doorOnTexture3;

        private static Texture doorOffTexture1;
        private static Texture doorOffTexture2;
        private static Texture doorOffTexture3;
        #endregion

        public bool InitEnabled;
        public bool Enabled;
        public int Key
        {
            get;
            private set;
        }

        private Texture doorOnTexture;
        private Texture doorOffTexture;

        private Sprite doorOnSprite;
        private Sprite doorOffSprite;

        static Door()
        {
            doorOnTexture1 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Yellow.png");
            doorOffTexture1 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Yellow_Off.png");
            doorOnTexture2 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Orange.png");
            doorOffTexture2 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Orange_Off.png");
            doorOnTexture3 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Green.png");
            doorOffTexture3 = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Green_Off.png");
        }
        public Door(Vector2 initPos, int key, bool initEnabled)
            : base(initPos)
        {
            InitEnabled = initEnabled;
            Key = key;

            switch (key)
            {
                case 1:
                    doorOnTexture = doorOnTexture1;
                    doorOffTexture = doorOffTexture1;
                    break;

                case 2:
                    doorOnTexture = doorOnTexture2;
                    doorOffTexture = doorOffTexture2;
                    break;

                case 3:
                    doorOnTexture = doorOnTexture3;
                    doorOffTexture = doorOffTexture3;
                    break;

                default:
                    throw new ArgumentException();
            } 

            float unitLength = MapManager.GetCurrentMap().UnitLength;
            doorOnSprite = new Sprite()
            {
                Texture = doorOnTexture,
                Position = Position,
                Size = new Vector2(unitLength, unitLength),
            };
            doorOffSprite = new Sprite()
            {
                Texture = doorOffTexture,
                Position = Position,
                Size = new Vector2(unitLength, unitLength),
            };
        }

        public override void Move(Vector2 pos)
        {
            base.Move(pos);
            doorOnSprite.Position = pos;
            doorOffSprite.Position = pos;
        }

        public override void Render(ref Matrix4 mat)
        {
            if (Enabled == true)
                doorOffSprite.Render(ref mat);
            else
                doorOnSprite.Render(ref mat);
        }
        public override void Update(Second deltaTime)
        {
        }
    }
}

