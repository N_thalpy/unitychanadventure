﻿using System;
using OpenTK;
using Tri;
using Tri.Mathematics;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.GameLoop;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.GameObject
{
    public class IceBlock : GameObjectBase
    {
        private static readonly Second Cooltime = (Second)0.12f;

        private bool enabled;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (value == enabled)
                    return;

                enabled = value;
                if (value == false)
                {
                    MapData md = MapManager.GetCurrentMap();
                    float unitLength = MapManager.GetCurrentMap().UnitLength;
                    int x = Mathf.Floor(Position.X / unitLength);
                    int y = Mathf.Floor(Position.Y / unitLength);

                    md.GameObjectDic.Remove(Mathf.Godel(x, y));
                    md[x, y] = TileType.None;
                }
            }
        }
        public bool UnderHeat;

        private static Texture iceTexture;
        private Sprite iceSprite;
        private Second timeRemain;

        static IceBlock()
        {
            iceTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Ice.png");
        }
        public IceBlock(Vector2 initPos)
            : base(initPos)
        {
            Enabled = true;
            UnderHeat = false;
            timeRemain = Cooltime;

            float unitLength = MainGameLoop.Instance.MapData.UnitLength;
            iceSprite = new Sprite()
            {
                Texture = iceTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
            };
        }

        public override void Move(Vector2 pos)
        {
            base.Move(pos);
            iceSprite.Position = pos;
        }

        public override void Update(Second deltaTime)
        {
            if (UnderHeat == true)
                timeRemain -= deltaTime;

            if (timeRemain < Second.Zero)
                Enabled = false;
        }
        public override void Render(ref Matrix4 mat)
        {
            if (Enabled == true)
                iceSprite.Render(ref mat);
        }
    }
}

