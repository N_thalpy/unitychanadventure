﻿using System;
using OpenTK;
using Tri.Mathematics;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.GameObject
{
    public class Ground : GameObjectBase
    {
        private float unitLength;

        private static Texture groundTexture;
        private Sprite groundSprite;

        static Ground()
        {
            groundTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/brushed_steel.png");
        }
        public Ground(Vector2 initPos)
            : base(initPos)
        {
            unitLength = MapManager.GetCurrentMap().UnitLength;
            groundSprite = new Sprite()
            {
                Texture = groundTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
            };
        }

        public override bool IsMovable()
        {
            return true;
        }
        public override void Move(Vector2 pos)
        {
            MapData md = MapManager.GetCurrentMap();

            int x = Mathf.Floor(Position.X / unitLength);
            int y = Mathf.Floor(Position.Y / unitLength);
            md.GameObjectDic.Remove(Mathf.Godel(x, y));
            TileType backup = md[x, y];
            md[x, y] = TileType.None;

            Position = pos;
            groundSprite.Position = Position;
            x = Mathf.Floor(Position.X / unitLength);
            y = Mathf.Floor(Position.Y / unitLength);
            md[x, y] = backup;
            md.GameObjectDic.Add(Mathf.Godel(x, y), this);
        }
        public override void Update(Tri.Second deltaTime)
        {
        }
        public override void Render(ref Matrix4 mat)
        {
            groundSprite.Render(ref mat);
        }
    }
}

