﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;
using Tri;
using Tri.Win.SystemComponent;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.GameLoop;

namespace UnitychanAdventure.GameObject
{
    public class Player : GameObjectBase
    {
        #region const
        private readonly Vector2 speed = new Vector2(200, 0);
        private readonly Vector2 jumpSpeed = new Vector2(0, 500);
        private readonly Vector2 gravity = new Vector2(0, -1200);
        private readonly float terminalSpeed = 600;

        private readonly Dictionary<PlayerState, AnimatedSprite> stateToAnimation;
        #endregion
        #region private variables
        enum PlayerState
        {
            Idle,
            Run,
            JumpUp,
            JumpFall,
            JumpLanding,
        }
        private PlayerState playerState;

        private Vector2 velocity;
        private Vector2 charSize;

        private static Texture[] idleTex;
        private static Texture[] runTex;
        private static Texture[] jumpUpTex;
        private static Texture[] jumpFallTex;
        private static Texture[] jumpLandingTex;

        private AnimatedSprite idleSprite;
        private AnimatedSprite runSprite;
        private AnimatedSprite jumpUpSprite;
        private AnimatedSprite jumpFallSprite;
        private AnimatedSprite jumpLandingSprite;

        // lrflag : 
        // 1 when facing to left
        // -1 when facing to right
        private int lrflag;

        #endregion

        static Player()
        {
            idleTex = new Texture[]
            {
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Idle_1.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Idle_2.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Idle_3.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Idle_4.png"),         
            };
            runTex = new Texture[]
            {
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_1.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_2.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_3.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_4.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_5.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_6.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_7.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_8.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_9.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_10.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_11.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_12.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_13.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_14.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_15.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_16.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_17.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Run_18.png"),
            };
            jumpUpTex = new Texture[]
            {
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Jump_Up_1.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Jump_Up_2.png"),
            };
        jumpFallTex = new Texture[]
            {
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Jump_Fall_1.png"),
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Jump_Fall_2.png"),
            };
            jumpLandingTex = new Texture[]
            {
                Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Jump_Landing.png"),
            };
        }
        public Player(Vector2 initPos)
            :base(initPos)
        {
            playerState = PlayerState.Idle;
            charSize = Texture.CreateFromBitmapPath("Resources/Image/UnityChan/Unitychan_Idle_1.png").Size;
            lrflag = 1;

            Vector2 playerAnchor = new Vector2(0.5f, 0.1f);

            Second frameInt = (Second)(1f / 10);

            #region Animated Sprite Initializing
            idleSprite = new AnimatedSprite()
            {
                TextureList = idleTex,
                Size = charSize,
                FrameInterval = frameInt,
                Anchor = playerAnchor,
            };
            runSprite = new AnimatedSprite()
            {
                TextureList = runTex,
                Size = charSize,
                FrameInterval = frameInt,
                Anchor = playerAnchor,
            };
            jumpUpSprite = new AnimatedSprite()
            {
                TextureList = jumpUpTex,
                Size = charSize,
                FrameInterval = frameInt,
                Anchor = playerAnchor,
            };
            jumpFallSprite = new AnimatedSprite()
            {
                TextureList = jumpFallTex,
                Size = charSize,
                FrameInterval = frameInt,
                Anchor = playerAnchor,
            };
            jumpLandingSprite = new AnimatedSprite()
            {
                TextureList = jumpLandingTex,
                Size = charSize,
                FrameInterval = frameInt,
                Anchor = playerAnchor,
            };
            #endregion

            stateToAnimation = new Dictionary<PlayerState, AnimatedSprite>();
            stateToAnimation.Add(PlayerState.Idle, idleSprite);
            stateToAnimation.Add(PlayerState.Run, runSprite);
            stateToAnimation.Add(PlayerState.JumpUp, jumpUpSprite);
            stateToAnimation.Add(PlayerState.JumpFall, jumpFallSprite);
            stateToAnimation.Add(PlayerState.JumpLanding, jumpLandingSprite);
        }

        public void OnAir()
        {
            switch (playerState)
            {
                case PlayerState.Idle:
                case PlayerState.Run:
                    playerState = PlayerState.JumpFall;
                    break;

                default:
                    break;
            }
        }
        public void OnGround(float ypos)
        {
            switch (playerState)
            {
                case PlayerState.JumpFall:
                    Position = new Vector2(Position.X, ypos);
                    playerState = PlayerState.JumpLanding;
                    break;

                default:
                    break;
            }
        }
        public void OnHeadBlocked(float ypos)
        {
            Position = new Vector2(Position.X, ypos);
            velocity = new Vector2(velocity.X, 0);
        }
        public void OnSideBlocked(float xpos)
        {
            Position = new Vector2(xpos, Position.Y);
        }

        public override void Update(Second deltaTime)
        {
            Vector2 prevVelocity = velocity;
            velocity = new Vector2(0, velocity.Y);

            #region key inputs
            if (MainGameLoop.Instance.SerifManager.Focused == false)
            {
                if (InputHelper.IsDown(Key.Left))
                    velocity -= speed;
                if (InputHelper.IsDown(Key.Right))
                    velocity += speed;
                if (InputHelper.IsPressed(Key.Space))
                {
                    switch (playerState)
                    {
                        case PlayerState.Idle:
                        case PlayerState.Run:
                            playerState = PlayerState.JumpUp;
                            velocity += jumpSpeed;
                            break;

                        default:
                            break;
                    }
                }
            }
            #endregion
            #region gravity
            switch (playerState)
            {
                case PlayerState.JumpFall:
                case PlayerState.JumpLanding:
                case PlayerState.JumpUp:
                    velocity += gravity * deltaTime;
                    break;

                default:
                    break;
            }

            // terminal velocity
            if (velocity.Y < -terminalSpeed)
                velocity = new Vector2(velocity.X, -terminalSpeed);
            #endregion

            if (velocity.X != 0)
                lrflag = Math.Sign(velocity.X);
            switch (playerState)
            {
                case PlayerState.Idle:
                case PlayerState.Run:
                    if (velocity != Vector2.Zero)
                    {
                        if (playerState != PlayerState.Run)
                            runSprite.Reset(); 
                        playerState = PlayerState.Run;
                    }
                    else
                    {
                        if (prevVelocity.X != 0)
                            lrflag = Math.Sign(prevVelocity.X);

                        if (playerState != PlayerState.Idle)
                            idleSprite.Reset();
                        playerState = PlayerState.Idle;
                    }
                    break;

                case PlayerState.JumpUp:
                    if (velocity.Y < 0)
                        playerState = PlayerState.JumpFall;
                    break;

                case PlayerState.JumpLanding:
                    velocity = new Vector2(velocity.X, 0);
                    break;

                default:
                    break;
            }

            Position += velocity * deltaTime;

            AnimatedSprite asp = stateToAnimation[playerState];
            asp.Size = new Vector2(Math.Abs(asp.Size.X) * lrflag, asp.Size.Y);
            asp.Position = Position;
            asp.Update(deltaTime);
        }
        public override void Render(ref Matrix4 mat)
        {
            stateToAnimation[playerState].Render(ref mat);
            if (playerState == PlayerState.JumpLanding)
                playerState = PlayerState.Idle;
        }
    }
}

