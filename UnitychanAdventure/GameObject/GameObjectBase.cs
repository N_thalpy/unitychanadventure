﻿using System;
using OpenTK;
using Tri;
using Tri.Mathematics;
using UnitychanAdventure.Map;
using System.Diagnostics;
using Tri.Win;

namespace UnitychanAdventure.GameObject
{
    public abstract class GameObjectBase
    {
        public Vector2 Position;

        public GameObjectBase(Vector2 position)
        {
            Position = position;
        }

        public abstract void Update(Second deltaTime);
        public abstract void Render(ref Matrix4 mat);

        public virtual bool IsMovable()
        {
            return true;
        }
        public virtual void Move(Vector2 pos)
        {
            Debug.Assert(IsMovable() == true);
            float unitLength = MapManager.GetCurrentMap().UnitLength;
            MapData md = MapManager.GetCurrentMap();

            int x = Mathf.Floor(Position.X / unitLength);
            int y = Mathf.Floor(Position.Y / unitLength);
            md.GameObjectDic.Remove(Mathf.Godel(x, y));
            TileType backup = md[x, y];
            md[x, y] = TileType.None;

            Position = pos;
            x = Mathf.Floor(Position.X / unitLength);
            y = Mathf.Floor(Position.Y / unitLength);
            md[x, y] = backup;
            md.GameObjectDic.Add(Mathf.Godel(x, y), this);
        }
    }
}

