﻿using System;
using OpenTK;
using Tri;
using Tri.Win.Rendering;
using Tri.Win.UI;
using Tri.Mathematics;
using UnitychanAdventure.Map;
using UnitychanAdventure.GameLoop;

namespace UnitychanAdventure.GameObject
{
    public class LaserSensor : GameObjectBase
    {
        private Texture sensorTexture;
        private Sprite sensorSprite;

        public bool Enabled;
        public int Key
        {
            get;
            private set;
        }

        public LaserSensor(Vector2 initPos, int key)
            : base(initPos)
        {
            Key = key;

            float unitLength = MainGameLoop.Instance.MapData.UnitLength;
            switch (key)
            {
                case 1:
                    sensorTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/TempSensor_Yellow.png");                    
                    break;

                case 2:
                    sensorTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/TempSensor_Orange.png");
                    break;

                case 3:
                    sensorTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/TempSensor_Green.png");
                    break;

                default:
                    throw new ArgumentException();
            }
            sensorSprite = new Sprite()
            {
                Texture = sensorTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
            };
        }

        public override void Move(Vector2 pos)
        {
            base.Move(pos);
            sensorSprite.Position = pos;
        }

        public override void Update(Second deltaTime)
        {
            MapData md = MainGameLoop.Instance.MapData;
            foreach (GameObjectBase obj in md.GameObjectDic.Values)
                if (obj is Door)
                {
                    Door d = obj as Door;
                    if (d.Key == Key)
                        d.Enabled = d.InitEnabled ^ Enabled;
                }
        }
        public override void Render(ref Matrix4 mat)
        {
            sensorSprite.Render(ref mat);
        }
    }
}

