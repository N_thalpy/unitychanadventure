﻿using System;
using OpenTK;
using Tri.Mathematics;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.GameObject
{
    public class GlassBlock : GameObjectBase
    {
        private float unitLength;

        private static Texture glassTexture;
        private Sprite glassSprite;

        static GlassBlock()
        {
            glassTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Wall_Glass.png");
        }
        public GlassBlock(Vector2 initPos)
            : base(initPos)
        {
            unitLength = MapManager.GetCurrentMap().UnitLength;
            glassSprite = new Sprite()
            {
                Texture = glassTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
            };
        }

        public override bool IsMovable()
        {
            return true;
        }
        public override void Move(Vector2 pos)
        {
            MapData md = MapManager.GetCurrentMap();

            int x = Mathf.Floor(Position.X / unitLength);
            int y = Mathf.Floor(Position.Y / unitLength);
            md.GameObjectDic.Remove(Mathf.Godel(x, y));
            TileType backup = md[x, y];
            md[x, y] = TileType.None;

            Position = pos;
            glassSprite.Position = Position;
            x = Mathf.Floor(Position.X / unitLength);
            y = Mathf.Floor(Position.Y / unitLength);
            md[x, y] = backup;
            md.GameObjectDic.Add(Mathf.Godel(x, y), this);
        }
        public override void Update(Tri.Second deltaTime)
        {
        }
        public override void Render(ref Matrix4 mat)
        {
            glassSprite.Render(ref mat);
        }
    }
}

