﻿using System;
using System.Collections.Generic;
using OpenTK;
using Tri;
using Tri.Win;
using Tri.Mathematics;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.GameObject
{
    public class Piston : GameObjectBase
    {
        private static Second coolTime = (Second)0.7f;

        private Direction direction;
        private Vector2 vecDir;
        private float unitLength;

        private static Texture pistonTexture;
        private static Texture pistonOffTexture;
        private static Texture pistonBodyTexture;
        private static Texture pistonDirectionTexture;

        private Sprite pistonSprite;
        private List<Sprite> pistonBodySpriteList;
        private Sprite pistonDirectionSprite;

        private Second timeRemain;

        private GameObjectBase attached;

        private bool enabled;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (enabled == value)
                    return;

                GameSystem.LogSystem.WriteLine("Piston toggled to {0}", value);
                enabled = value;
                timeRemain = coolTime;
            }
        }
        public bool Controllable
        {
            get
            {
                return timeRemain < Second.Zero;
            }
        }

        static Piston()
        {
            pistonTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Spring_Body.png");
            pistonOffTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Spring_Body_Off.png");
            pistonBodyTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Spring.png");
            pistonDirectionTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/Spring_Direction.png");
        }
        public Piston(Vector2 initPos, Direction direction, bool initEnabled)
            : base(initPos)
        {
            this.direction = direction;
            enabled = initEnabled;
            this.unitLength = MapManager.GetCurrentMap().UnitLength;

            Degree angle = Degree.Zero;
            switch (direction)
            {
                case Direction.VerticalPos:
                    angle = (Degree)0;
                    vecDir = new Vector2(0, 1);
                    break;

                case Direction.VerticalNeg:
                    angle = (Degree)180;
                    vecDir = new Vector2(0, -1);
                    break;

                case Direction.HorizontalNeg:
                    angle = (Degree)90;
                    vecDir = new Vector2(-1, 0);
                    break;

                case Direction.HorizontalPos:
                    angle = (Degree)270;
                    vecDir = new Vector2(1, 0);
                    break;

                default:
                    throw new NotImplementedException();
            }

            pistonSprite = new Sprite()
            {
                Texture = pistonTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
            };
            pistonDirectionSprite = new Sprite()
            {
                Texture = pistonDirectionTexture,
                Position = initPos,
                Size = new Vector2(unitLength, unitLength),
                Angle = angle,
            };
            pistonBodySpriteList = new List<Sprite>();
        }

        public override bool IsMovable()
        {
            return false;
        }

        public override void Render(ref Matrix4 mat)
        {
            pistonSprite.Render(ref mat);
            foreach (Sprite sp in pistonBodySpriteList)
                sp.Render(ref mat);
            pistonDirectionSprite.Render(ref mat);
        }
        public override void Update(Second deltaTime)
        {
            timeRemain -= deltaTime;

            if (Controllable == true)
                pistonSprite.Texture = pistonTexture;
            else
                pistonSprite.Texture = pistonOffTexture;

            foreach (Sprite sp in pistonBodySpriteList)
                sp.Dispose();
            pistonBodySpriteList.Clear();

            pistonDirectionSprite.Position = pistonSprite.Position;
            if (Enabled == true)
            {
                Vector2 pos = new Vector2(Mathf.Floor(Position.X / unitLength), Mathf.Floor(Position.Y / unitLength));
                Degree angle = Degree.Zero;
                switch (direction)
                {
                    case Direction.VerticalPos:
                    case Direction.VerticalNeg:
                        break;

                    case Direction.HorizontalPos:
                    case Direction.HorizontalNeg:
                        angle = (Degree)90;
                        break;

                    default:
                        throw new NotImplementedException();
                }

                pos += vecDir;
                MapData md = MapManager.GetCurrentMap();

                bool isCollided = false;
                while (true)
                {
                    if (attached == null)
                    {
                        if (pos.X < 0 || pos.Y < 0)
                            break;
                        if (pos.X >= md.Width || pos.Y >= md.Height)
                        {
                            do
                            {
                                pistonBodySpriteList.Add(
                                    new Sprite()
                                    {
                                        Texture = pistonBodyTexture,
                                        Size = new Vector2(unitLength, unitLength),
                                        Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                        Angle = angle,
                                    });
                                pos += vecDir;
                            } while (pos.X * unitLength < GameSystem.MainForm.Viewport.X &&
                                     pos.Y * unitLength < GameSystem.MainForm.Viewport.Y);
                            break;
                        }

                        switch (md[(int)pos.X, (int)pos.Y])
                        {
                            case TileType.None:
                                pistonBodySpriteList.Add(
                                    new Sprite()
                                    {
                                        Texture = pistonBodyTexture,
                                        Size = new Vector2(unitLength, unitLength),
                                        Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                        Angle = angle,
                                    });
                                pistonDirectionSprite.Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f);
                                break;

                            case TileType.IceBlock:
                                if ((md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)] as IceBlock).Enabled == true)
                                {
                                    GameSystem.LogSystem.WriteLine("Collided w/ {0}, x{1}, y{2}", md[(int)pos.X, (int)pos.Y], (int)pos.X, (int)pos.Y);
                                    attached = md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)];
                                    GameSystem.LogSystem.WriteLine("Attached {0}", attached.GetType().Name);
                                    isCollided = true;
                                }
                                break;   

                            default:
                                GameSystem.LogSystem.WriteLine("Collided w/ {0}, x{1}, y{2}", md[(int)pos.X, (int)pos.Y], (int)pos.X, (int)pos.Y);
                                attached = md.GameObjectDic[Mathf.Godel((int)pos.X, (int)pos.Y)];
                                GameSystem.LogSystem.WriteLine("Attached {0}", attached.GetType().Name);
                                isCollided = true;
                                break;       
                        }
                    }
                    else
                    {
                        if (pos.X + vecDir.X < 0 || pos.Y + vecDir.Y < 0)
                            break;
                        if (pos.X + vecDir.X >= md.Width || pos.Y + vecDir.Y >= md.Height)
                            break;

                        switch (md[(int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y)])
                        {
                            case TileType.None:
                                pistonBodySpriteList.Add(
                                    new Sprite()
                                    {
                                        Texture = pistonBodyTexture,
                                        Size = new Vector2(unitLength, unitLength),
                                        Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                        Angle = angle,
                                    });
                                pistonDirectionSprite.Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f);
                                break;

                            case TileType.IceBlock:
                                if ((md.GameObjectDic[Mathf.Godel((int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y))] as IceBlock).Enabled == true)
                                {
                                    isCollided = true;
                                    if (md.GameObjectDic.ContainsKey(Mathf.Godel((int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y))) == true)
                                    if (md.GameObjectDic[Mathf.Godel((int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y))] == attached)
                                    {
                                        pistonBodySpriteList.Add(
                                            new Sprite()
                                            {
                                                Texture = pistonBodyTexture,
                                                Size = new Vector2(unitLength, unitLength),
                                                Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                                Angle = angle,
                                            });
                                        pistonDirectionSprite.Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f);
                                        isCollided = false;
                                    }
                                }
                                break;
                                

                            default:
                                isCollided = true;
                                if (md.GameObjectDic.ContainsKey(Mathf.Godel((int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y))) == true)
                                if (md.GameObjectDic[Mathf.Godel((int)(pos.X + vecDir.X), (int)(pos.Y + vecDir.Y))] == attached)
                                {
                                    pistonBodySpriteList.Add(
                                        new Sprite()
                                        {
                                            Texture = pistonBodyTexture,
                                            Size = new Vector2(unitLength, unitLength),
                                            Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f),
                                            Angle = angle,
                                        });
                                    pistonDirectionSprite.Position = unitLength * new Vector2(pos.X + 0.5f, pos.Y + 0.5f);
                                    isCollided = false;
                                }
                                break;
                        }
                    }
                    if (isCollided == true)
                        break;
                    
                    pos += vecDir;
                }
            }

            if (attached != null)
            if (attached.IsMovable() == true)
                attached.Move(pistonDirectionSprite.Position + vecDir * unitLength);
        }
    }
}

