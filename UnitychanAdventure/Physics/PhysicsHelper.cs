﻿using System;
using Tri.Mathematics;
using UnitychanAdventure.GameObject;
using UnitychanAdventure.Map;

namespace UnitychanAdventure.Physics
{
    public class PhysicsHelper
    {
        private MapData md;
        
        public PhysicsHelper(MapData md)
        {
            this.md = md;
        }

        private bool IsNonMovableBlock(int x, int y)
        {
            switch (md[x, y])
            {
                case TileType.Ground:
                case TileType.GlassBlock:
                    return true;

                case TileType.IceBlock:
                    if ((md.GameObjectDic[Mathf.Godel(x, y)] as IceBlock).Enabled == true)
                        return true;
                    else
                        return false;

                case TileType.Door1:
                case TileType.Door2:
                case TileType.Door3:
                case TileType.Door1Rev:
                case TileType.Door2Rev:
                case TileType.Door3Rev:
                    if ((md.GameObjectDic[Mathf.Godel(x, y)] as Door).Enabled == true)
                        return false;
                    else
                        return true;

                default:
                    return false;
            }
        }
        public bool IsPlayerOnGround(Player pl)
        {
            int xleft = Mathf.Floor(pl.Position.X / md.UnitLength - 0.3f);
            int xright = Mathf.Floor(pl.Position.X / md.UnitLength + 0.3f);
            int y = Mathf.Floor(pl.Position.Y / md.UnitLength);
            
            if (y < 0 || y >= md.Height)
                return false;
            
            if (0 <= xleft && xleft < md.Width)
            if (IsNonMovableBlock(xleft, y) == true)
                return true;

            if (0 <= xright && xright < md.Width)
            if (IsNonMovableBlock(xright, y) == true)
                return true;

            return false;
        }
        public bool IsPlayerHeadBlocked(Player pl, float align)
        {
            int x = Mathf.Floor(pl.Position.X / md.UnitLength);
            int y = Mathf.Floor((pl.Position.Y + align) / md.UnitLength);

            if (x < 0 || x >= md.Width)
                return false;
            if (y < 0 || y >= md.Height)
                return false;

            if (IsNonMovableBlock(x, y) == true)
                return true;
            return false;
        }
        public bool IsPlayerRightSideBlocked(Player pl)
        {
            int x = Mathf.Floor(pl.Position.X / md.UnitLength + 0.5f);
            int y = Mathf.Floor(pl.Position.Y / md.UnitLength + 0.02f);
            if (x < 0 || x >= md.Width)
                return true;
            if (y < 0 || y >= md.Height)
                return false;

            if (IsNonMovableBlock(x, y) == true)
                return true;
            return false;
        }
        public bool IsPlayerLeftSideBlocked(Player pl)
        {
            int x = Mathf.Floor(pl.Position.X / md.UnitLength - 0.5f);
            int y = Mathf.Floor(pl.Position.Y / md.UnitLength + 0.02f);
            if (x < 0 || x >= md.Width)
                return true;
            if (y < 0 || y >= md.Height)
                return false;

            if (IsNonMovableBlock(x, y) == true)
                return true;
            return false;
        }
    }
}

