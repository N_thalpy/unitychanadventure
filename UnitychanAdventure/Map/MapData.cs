﻿using System;
using System.Collections.Generic;
using OpenTK;
using Tri;
using Tri.Mathematics;
using Tri.Win;
using Tri.Win.Rendering;
using Tri.Win.UI;
using UnitychanAdventure.GameObject;
using UnitychanAdventure.Serif;

namespace UnitychanAdventure.Map
{
    public enum TileType
    {
        None,
        Ground,
        IceBlock,
        GlassBlock,

        LaserHorizontalPos,
        LaserHorizontalNeg,
        LaserVerticalPos,
        LaserVerticalNeg,
        LaserHorizontalPosRev,
        LaserHorizontalNegRev,
        LaserVerticalPosRev,
        LaserVerticalNegRev,

        PistonHorizontalPos,
        PistonHorizontalNeg,
        PistonVerticalPos,
        PistonVerticalNeg,
        PistonHorizontalPosRev,
        PistonHorizontalNegRev,
        PistonVerticalPosRev,
        PistonVerticalNegRev,

        LaserSensor1,
        Door1,
        Door1Rev,

        LaserSensor2,
        Door2,
        Door2Rev,

        LaserSensor3,
        Door3,
        Door3Rev,

        Goal,
    }

    public class MapData
    {
        #region static
        private static Texture goalTexture;
        #endregion

        #region public
        public Dictionary<int, GameObjectBase> GameObjectDic;

        // y, x
        // ^
        // |
        // |
        // | origin
        // +--------->

        private TileType[,] savedData;
        private TileType[,] tileData;
        public readonly int Width;
        public readonly int Height;
        public readonly float UnitLength;

        public readonly Vector2 PlayerPos;

        // returns true: update other routines
        // returns false: do not update other routines (ex. player moving, ...) 
        public Func<bool> UpdateAction;

        public Action PreRenderAction;
        public Action ProRenderAction;
        public List<SerifData> SerifList;
        #endregion

        public TileType this[int x, int y]
        {
            get
            {
                return tileData[Height - 1 - y, x];
            }
            set
            {
                tileData[Height - 1 - y, x] = value;
            }
        }

        static MapData()
        {
            goalTexture = Texture.CreateFromBitmapPath("Resources/Image/Tiles/portal_no_base.png");
        }
        public MapData(TileType[,] tileData, Vector2 playerPos, List<SerifData> serifList)
        {
            PlayerPos = playerPos;
            savedData = tileData;
            SerifList = serifList;
            this.tileData = CloneData(savedData);

            Width = tileData.GetLength(1);
            Height = tileData.GetLength(0);
            UnitLength = GameSystem.MainForm.Viewport.X / Width;
        }

        private TileType[,] CloneData(TileType[,] data)
        {
            TileType[,] rv = new TileType[data.GetLength(0), data.GetLength(1)];
            for (int x = 0; x < data.GetLength(0); x++)
                for (int y = 0; y < data.GetLength(1); y++)
                    rv[x, y] = data[x, y];

            return rv;
        }

        public void InitGameObjects()
        {
            // Dictionary initialization
            GameObjectDic = new Dictionary<int, GameObjectBase>();
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    Vector2 pos = new Vector2(UnitLength * (x + 0.5f), UnitLength * (y + 0.5f));
                    switch (tileData[Height - 1 - y, x])
                    {
                        case TileType.None:
                        case TileType.Goal:
                            break;

                        case TileType.GlassBlock:
                            GameObjectDic.Add(Mathf.Godel(x, y), new GlassBlock(pos));
                            break;

                        case TileType.Ground:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Ground(pos));
                            break;

                        case TileType.IceBlock:
                            GameObjectDic.Add(Mathf.Godel(x, y), new IceBlock(pos));
                            break;

                        #region LaserEmitters
                        case TileType.LaserHorizontalPos:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.HorizontalPos, false));
                            break;

                        case TileType.LaserHorizontalPosRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.HorizontalPos, true));
                            break;

                        case TileType.LaserHorizontalNeg:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.HorizontalNeg, false));
                            break;

                        case TileType.LaserHorizontalNegRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.HorizontalNeg, true));
                            break;

                        case TileType.LaserVerticalPos:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.VerticalPos, false));
                            break;

                        case TileType.LaserVerticalPosRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.VerticalPos, true));
                            break;

                        case TileType.LaserVerticalNeg:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.VerticalNeg, false));
                            break;

                        case TileType.LaserVerticalNegRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserEmitter(pos, Direction.VerticalNeg, true));
                            break;
                        #endregion
                        #region Piston
                        case TileType.PistonHorizontalPos:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.HorizontalPos, false));
                            break;

                        case TileType.PistonHorizontalPosRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.HorizontalPos, true));
                            break;

                        case TileType.PistonHorizontalNeg:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.HorizontalNeg, false));
                            break;

                        case TileType.PistonHorizontalNegRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.HorizontalNeg, true));
                            break;

                        case TileType.PistonVerticalPos:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.VerticalPos, false));
                            break;

                        case TileType.PistonVerticalPosRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.VerticalPos, true));
                            break;

                        case TileType.PistonVerticalNeg:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.VerticalNeg, false));
                            break;

                        case TileType.PistonVerticalNegRev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Piston(pos, Direction.HorizontalNeg, true));
                            break;

                        #endregion
                        #region Sensor
                        case TileType.LaserSensor1:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserSensor(pos, 1));
                            break;
                        
                        case TileType.LaserSensor2:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserSensor(pos, 2));
                            break;
                        
                        case TileType.LaserSensor3:
                            GameObjectDic.Add(Mathf.Godel(x, y), new LaserSensor(pos, 3));
                            break;
                        #endregion
                        #region Door
                        case TileType.Door1:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 1, false));
                            break;
                        
                        case TileType.Door2:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 2, false));
                            break;
                        
                        case TileType.Door3:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 3, false));
                            break;

                        case TileType.Door1Rev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 1, true));
                            break;
 
                        case TileType.Door2Rev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 2, true));
                            break;

                        case TileType.Door3Rev:
                            GameObjectDic.Add(Mathf.Godel(x, y), new Door(pos, 3, true));
                            break;
                        #endregion

                        default:
                            throw new NotImplementedException();
                    }
                }
        }
        public void Reset()
        {
            tileData = CloneData(savedData);
            GameObjectDic.Clear();
            InitGameObjects();
        }

        public void Update(Second deltaTime)
        {
            foreach (GameObjectBase obj in GameObjectDic.Values)
                if (obj is LaserSensor)
                {
                    LaserSensor ls = obj as LaserSensor;
                    ls.Enabled = false;
                }
            
            List<GameObjectBase> objList = new List<GameObjectBase>();
            foreach (GameObjectBase obj in GameObjectDic.Values)
                if (obj is LaserEmitter)
                    objList.Add(obj);
            foreach (GameObjectBase obj in GameObjectDic.Values)
                if (obj is LaserSensor)
                    objList.Add(obj);
            foreach (GameObjectBase obj in GameObjectDic.Values)
                if (obj is Door)
                    objList.Add(obj);
            foreach (GameObjectBase obj in GameObjectDic.Values)
                if (objList.Contains(obj) == false)
                    objList.Add(obj);
            
            foreach (GameObjectBase obj in objList)
                obj.Update(deltaTime);
        }
        public void Render(ref Matrix4 mat)
        {
            List<Sprite> miscSpriteList = new List<Sprite>();
            List<GameObjectBase> miscObjectList = new List<GameObjectBase>();
            List<GameObjectBase> laserObjectList = new List<GameObjectBase>();

            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                    switch (tileData[Height - 1 - y, x])
                    {
                        case TileType.None:
                            break;

                        case TileType.Goal:
                            miscSpriteList.Add(new Sprite()
                            {
                                Texture = MapData.goalTexture,
                                Size = new Vector2(1, 2) * UnitLength,
                                Position = new Vector2(x * UnitLength, y * UnitLength),
                                Anchor = Vector2.Zero,
                            });
                            break;

                        case TileType.Ground:
                        case TileType.GlassBlock:
                        case TileType.IceBlock:
                        case TileType.Door1:
                        case TileType.Door2:
                        case TileType.Door3:
                        case TileType.Door1Rev:
                        case TileType.Door2Rev:
                        case TileType.Door3Rev:
                        case TileType.PistonHorizontalNeg:
                        case TileType.PistonHorizontalPos:
                        case TileType.PistonHorizontalNegRev:
                        case TileType.PistonHorizontalPosRev:
                        case TileType.PistonVerticalNeg:
                        case TileType.PistonVerticalNegRev:
                        case TileType.PistonVerticalPos:
                        case TileType.PistonVerticalPosRev:
                            miscObjectList.Add(GameObjectDic[Mathf.Godel(x, y)]);
                            break;
                        
                        case TileType.LaserHorizontalPos:
                        case TileType.LaserHorizontalNeg:
                        case TileType.LaserVerticalPos:
                        case TileType.LaserVerticalNeg:
						case TileType.LaserHorizontalPosRev:
						case TileType.LaserHorizontalNegRev:
						case TileType.LaserVerticalPosRev:
						case TileType.LaserVerticalNegRev:
                        case TileType.LaserSensor1:
                        case TileType.LaserSensor2:
                        case TileType.LaserSensor3:
                            laserObjectList.Add(GameObjectDic[Mathf.Godel(x, y)]);
                            break;

                        default:
                            throw new NotImplementedException();
                    }

            foreach (Sprite sp in miscSpriteList)
            {
                sp.Render(ref mat);
                sp.Dispose();
            }
            foreach (GameObjectBase obj in miscObjectList)
                obj.Render(ref mat);
            foreach (GameObjectBase obj in laserObjectList)
                obj.Render(ref mat);
        }
    }
}

