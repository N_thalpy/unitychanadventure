﻿using System;
using System.Collections.Generic;
using OpenTK;
using UnitychanAdventure.Serif;

namespace UnitychanAdventure.Map
{
	public class MapDataFactory
	{
		private static TileType N = TileType.None;
		private static TileType G = TileType.Ground;
		private static TileType L1 = TileType.LaserVerticalPos;
		private static TileType L2 = TileType.LaserHorizontalPos;
		private static TileType L3 = TileType.LaserVerticalNeg;
		private static TileType L4 = TileType.LaserHorizontalNeg;
		private static TileType L5 = TileType.LaserVerticalPosRev;
		private static TileType L6 = TileType.LaserHorizontalPosRev;
		private static TileType L7 = TileType.LaserVerticalNegRev;
		private static TileType L8 = TileType.LaserHorizontalNegRev;
		private static TileType S1 = TileType.LaserSensor1;
		private static TileType S2 = TileType.LaserSensor2;
		private static TileType S3 = TileType.LaserSensor3;
		private static TileType F = TileType.Goal;
		private static TileType D1 = TileType.Door1;
		private static TileType D2 = TileType.Door2;
		private static TileType D3 = TileType.Door3;
		private static TileType R1 = TileType.Door1Rev;
		private static TileType R2 = TileType.Door2Rev;
		private static TileType R3 = TileType.Door3Rev;
		private static TileType I = TileType.IceBlock;
		private static TileType GL = TileType.GlassBlock;
        private static TileType P1 = TileType.PistonVerticalPos;
		private static TileType P2 = TileType.PistonHorizontalPos;
		private static TileType P3 = TileType.PistonVerticalNeg;
		private static TileType P4 = TileType.PistonHorizontalNeg;

		public static MapData TestScene = new MapData(
			new TileType[,] {
				{N, N, N, N, N, N, N, G, G, G, G, N, N, N, N, N, S1,D1,N, N, N, N, N, N, N, N},
				{N, N, N, N, N, N, N, G, G, G, G, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N},
				{N, N, N, N, N, N, N, G, G, G, G, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N},
				{N, N, N, N, N, N, N, G, G, G, G, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N},
				{N, N, N, G, G, N, N, N, N, N, N, N, N, N, N, N, L1,G, N, N, N, L2,N, F, N, N},
				{G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N},
        }, new Vector2(0, 1), new List<SerifData>());

		public static MapData Tutorial00 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N, N, N, N, N, N, N, N, N, N },  
				{ N, N, N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
        }, new Vector2(1, 1), new List<SerifData>() {
            new SerifData(SerifData.unityChanSurpriseTexture, "앗 여기는 어디지!"),
            new SerifData(SerifData.unityChanHappyTexture, "모르겠으니 일단 밖으로 나가자."),
            new SerifData(SerifData.unityChanHappyTexture, "나를 왼쪽, 오른쪽 방향키와 스페이스바로 움직여서\n문이 있는 곳 까지 이동시켜줘!"),
        });

		public static MapData Tutorial01 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, G, G, G, N, N, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, G, G, G, N, N, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G },
        }, new Vector2(3, 3), new List<SerifData>() {
        	new SerifData(SerifData.unityChanHappyTexture, "아, 만약 스테이지를 리셋하고 싶다면\nR 키를 눌러서 리셋해줘."),	
    	});

		public static MapData Tutorial02 = new MapData (
			new TileType[,] {
				{ N, N, N, S1,N, N, N, N, N, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, L1,N, N, N, N, N, N, N, N, N, N, N, D1,N, N, N, F, N, N, N, N, N, N },
				{ G, G, G, G, G, N, N, G, G, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G, G, G },
        }, new Vector2(9, 1), new List<SerifData>() {
        	new SerifData(SerifData.unityChanHappyTexture, "저 빨간색 세모는 레이저를 쏘는 장치인 것 같아."),
        	new SerifData(SerifData.unityChanSmileTexture, "저 마름모 모양 센서에 맞추면 타일이 열리지 않을까?")	,
        	new SerifData(SerifData.unityChanHappyTexture, "Z키를 눌러서 저 레이저를 켜보자!"),
    	});

		public static MapData Tutorial03 = new MapData (
			new TileType[,] {
				{ N, N, S2,N, N, D1,N, N, N, N, N, N, N, S1,N, N, D2,N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N, D2,N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N, D2,N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, D1,N, N, N, N, N, N, N, N, N, N, D2,N, N, N, N, N, N, N, N, N },
				{ N, N, L1,N, N, D1,N, N, N, N, N, N, N, N, N, N, D2,N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, N, N, N, N, N, N, N, L1,N, N, D2,N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, D2,N, N, N, F, N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
        }, new Vector2(7, 2), new List<SerifData>());
				
		public static MapData Tutorial04 = new MapData (
			new TileType[,] {
				{ N, N, N, L2,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N, S1},
				{ N, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, R1,R1,R1,N, N },
				{ N, N, N, N, N, N, L2,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, S2},
				{ N, N, N, N, G, G, G, N, N, N, N, N, N, N, N, N, N, N, R2,R2,R2,N, N, N, N, N },
				{ S3,N, N, N, N, N, N, N, N, L4,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, G, G, G, N, N, N, N, N, R3,R3,R3,N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N },
        }, new Vector2 (2, 7), new List<SerifData>());

		public static MapData Tutorial05 = new MapData (
			new TileType[,] {
				{ G, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, N, G, N, N, N, N, N, F, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, G, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, G, N, N, N, N, G, G, N, N, N, N, N, N, N, N, N, N, N },
				{ G, L2,N, N, N, N, I, I, I, N, N, N, G, G, G, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, N, N, N, N, N, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N },
        }, new Vector2 (1, 6), new List<SerifData>() {
        	new SerifData(SerifData.unityChanHappyTexture, "저기 있는 하늘색 블럭은 얼음인것같아."),
        	new SerifData(SerifData.unityChanHappyTexture, "뭔가 녹일 수 있는 방법이 있지 않을까?"),	
    	});
		
		public static MapData Tutorial06 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, G, G, N, N, G, G, G, G, G, G, I, G, G, N, N, N, N, F, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, G, G, G, G, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, N, N, N, N, N, N, N, N, N, L1,N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, N },
        }, new Vector2(9, 5), new List<SerifData>());
				
		public static MapData Tutorial07 = new MapData (
			new TileType[,] {
				{ G, N, N, N, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, N, N, N, N, N, N, N, N, N, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, L2,N, N, N, N, I, I, I, I, I, S1,N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, N, N, N, N, N, R1,N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, N, N, N, N, N, R1,N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, N, N, N, N, N, R1,N, N, N, N, N, F, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N },
        }, new Vector2 (4, 4), new List<SerifData>());

		public static MapData Tutorial08 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, I, G, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, I, G, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N, N, N, N, N, N, N, N, I, G, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, G, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, G, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, I, G, N },
				{ G, L2,N, N, I, I, I, I, I, I, I, I, I, I, I, I, I, S1,N, N, N, N, N, L1,G, N },
				{ G, D1,D1,D1,D1,D1,D1,G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N },
        }, new Vector2 (1, 1), new List<SerifData>());
				
		public static MapData Tutorial09 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, L2,N, N, N, N, N, GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, S1,N, N },
				{ G, G, G, G, G, G, GL,GL,GL,GL,GL,GL,GL,D1,D1,D1,G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, N, N, N, F, N, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
        }, new Vector2 (20, 7), new List<SerifData>() {
        	new SerifData(SerifData.unityChanHappyTexture, "저기 있는 흰색 블럭은 유리인것같아."),
        	new SerifData(SerifData.unityChanHappy2Texture, "저기로 레이저를 통과시키면 문까지 갈 수 있지 않을까?"),	
    	});

		public static MapData Tutorial10 = new MapData (
			new TileType[,] {

				{ N, N, N, N, N, N, N, GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, L3,GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,I, GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,I, GL,GL,GL,N, N, N, N, N, F, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,I, GL,GL,GL,N, N, N, N, R2,R2,N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, R1,R1,N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, G, G, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, L3,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, S1,N, N, N, S2,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },

        }, new Vector2 (2, 11), new List<SerifData>());

		public static MapData Tutorial11 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, G, N, N, N, L7,N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ S1,N, N, N, N, N, N, N, N, N, N, N, N, N, P4,N, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, P1,G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
				{ N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, R1,R1,R1,R1,R1,R1,N, N, N, N },
			}, new Vector2 (6, 3), new List<SerifData> () {
				new SerifData(SerifData.unityChanHappyTexture, "저 보라색 물건은 피스톤인것 같아."),
				new SerifData(SerifData.unityChanHappyTexture, "물건을 움직이거나 하는데 쓸 수 있지 않을까?"),
			});

		public static MapData Tutorial12 = new MapData (
			new TileType[,] {
				{ N, N, P3,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, L4,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, D1,D1,D1,D1,D1,N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, D1,D1,D1,D1,D1,N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, N, F, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, N, G, G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, S1,G, G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },

			}, new Vector2 (4, 9), new List<SerifData> ());

		public static MapData Stage01 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, L3,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, G, G, N, N, N, N, N },
				{ N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N, G, G, N, N },
				{ S2,N, N, N, N, N, N, GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, L4},
				{ N, N, N, G, G, G, G, R2,G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ N, N, N, G, G, G, G, R2,G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N },
				{ L6,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, R2,R2,D1,D1,I, I, I, I, I, I },
				{ G, G, G, G, G, G, G, S1,G, G, G, G, G, G, G, G, R2,R2,D1,D1,N ,N, N, N, N, N },
				{ G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, R2,R2,D1,D1,N ,N, N, N, N, N },
        }, new Vector2 (5, 13), new List<SerifData>());
				
		public static MapData Stage02 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, N, N, L7,N, N, N, S2,N, N, N, N, N, N, N, N, R2,R2,R2},
				{ L6,N, N, N, N, N, N, N, N, N, D2,N, N, N, N, N, R3,N, N, N, N, N, N, N ,S1,R2},
				{ N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, R2,R2,R2},
				{ N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, N }, 
				{ N, N, N, N, F, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, D1,D1,D1,L2,N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, S3},
				{ N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, N, N, N, G, G, G, I, N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, N, N, N, N, N, N, I, R2,R2,N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, L1,N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, N, R1,R1,N, N, N, N, N, N },
				{ N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, D1,D1,N, N, N },
				{ N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, L1,N },
				{ N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
			}, new Vector2 (24, 1), new List<SerifData>());
		
		public static MapData Stage03 = new MapData (
			new TileType[,]{
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, N, N, N, N, F, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, N, G, N, G, N, N, N, G, N, N, N, G, N, N, N, G, N, N, N, G, N, G, N, G, N },
				{ L6,D1,GL,I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, S3},
				{ G, G, L2,I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, S1},
				{ D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3},
			}, new Vector2 (2, 1), new List<SerifData>() {
				new SerifData(SerifData.unityChanSurpriseTexture, "아! 그러고보니 생각난건데 레이저는 켜고 나서\n일정 시간이 지나야 끌 수 있는것같아."),
				new SerifData(SerifData.unityChanHappyTexture, "타이밍을 적당히 맞춰서 건드려야할 것 같아."),
			});

		public static MapData Stage04 = new MapData (
			new TileType[,] {
				{ S1,N, N, N, L7,L7,L7,L7,L7,D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, L7,N },
				{ N, N, N, N, D1,D1,D1,D1,D1,D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, D2,N },
				{ N, N, N, N, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ N, N, N, N, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ L1,N, N, N, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D2,D2,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ G, G, G, G, I, I, I, I, I, D3,D3,N, N, N, N, N, N, N, N, N, N, N, N, N, I, N },
				{ L2,N, N, N, N, N, N, GL,GL,I, I, I, I, I, I, I, I, I, I, I, F, I, I, I, I, S2},
				{ D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,N, N,R1,D1, N, N, N, G, G, G, G, N, N, S3,N },
        }, new Vector2 (2, 8), new List<SerifData>());
				
		public static MapData Stage05 = new MapData (
			new TileType[,] {
				{N, N, N, N, N, N, N, N, N, N, S2,N, S1,N, S3,N, N, N, N, N, N, N, N, N, N, F },
				{P2,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, I, I },
				{GL,GL,N, N, L2,N, N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, I, I },
				{GL,GL,N, N, GL,GL,N, N, N, N, N, N, I, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,N, N, GL,GL,N, N, I, N, N, N, N, N, N, N, N, N, R1,R1,N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, L1,N, N, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, N, N, R2,R2,N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, R3,R3,N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, I, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,I, I, GL,I, I, N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,D1,D1,D1,D1,D1,N, N, N, N, N, N, N, N, N, N, N },
				{GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,L5,L5,L5,L5,L5,N, N, N, N, N, N, N, N, N, N, N },
			}, new Vector2 (14, 9), new List<SerifData> ());

		public static MapData Stage06 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, N, N, N, S3,I, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, P2,N, N, N, N, N, I, I, N, N, N, N, N, N, N, N, S2,N, N, N, N, N, N, N },
				{ N, N, G, G, N, N, N, N, I, I, GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N },
				{ N, N, G, G, G, N, N, N, I, I, N, N, N, N, N, N, GL,N, N, N, N, N, N, N, N, F },
				{ N, S1,N, N, N, N, N, N, I, I, N, N, GL,GL,N, L4,GL,N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, G, G, I, I, GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, N, I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, D3},
				{ N, N, N, R1,R1,N, N, N, I, I, N, N, N, N, N, N, N, N, N, R2,R2,N, N, R1,R1,D3},
				{ P2,N, N, N, N, N, N, N, I, I, N, N, N, N, N, N, N, N, L1,R2,R2,N, N, N, N, N },
				{ G, G, G, N, N, N, N, N, L1,L1,N, N, N, N, N, G, G, G, G, N, N, N, N, N, N, N },
				{ G, G, G, N, N, G, G, G, G, G, G, G, G, N, N, G, G, G, G, N, N, N, N, N, G, G },
				{ N, N, N, N, N, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, N, N, G, G, G, G },
				{ N, N, N, N, N, G, G, G, G, G, G, G, G, N, N, N, N, N, N, N, G, G, G, G, G, G },
			}, new Vector2 (6, 3), new List<SerifData> ());

		public static MapData Stage07 = new MapData (
			new TileType[,] {
				{ G, G, G, G, S2,N, G, L7,L7,L7,GL,N, N, L7,L7,L7,L7,L7,N, N, N, N, N, N, N, N },
				{ G, G, G, G, N, N, G, D2,D2,D2,GL,N, N, D1,D1,D1,D1,D1,N, N, N, N, N, N, N, N },
				{ G, G, G, G, L1,N, N, N, N, N, N, N, N, R2,R2,R2,R2,R2,N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, N, N, N, GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ G, G, G, G, G, G, G, I, I, I, GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, N, N, N, N, N, I, I, I, GL,GL,GL,I, I, I, N, N, N, N, N, N, N, N, N, N },
				{ GL,N, N, N, N, GL,GL,GL,GL,GL,GL,GL,GL,I, I, I, I, N, N, N, S1,N, N, N, N, N },
				{ GL,GL,N, N, N, N, N, N, N, N, N, N, N, I, I, I, I, I, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,L2,N, N, N, N, N, N, N, N, N, I, I, S3,N, N, N, N, L1,N, N, N, N, N },
				{ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, N, N, N, N, N, N, G, G, G, G, G, G, G },
				{ D2,D3,D3,D3,D3,D3,D3,D3,D3,D3,D3,N, N, I, I, I, I, I, I, I, I, I, R1,R2,D3,L8},
				{ D2,D3,R1,R1,R1,R1,R1,R1,R1,R1,R1,N, N, I, I, I, I, I, I, I, I, I, R1,R2,D3,L8},
				{ D2,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N, N, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ D2,N, N, N, N, N, N, N, N, F, GL,N, N, G, G, G, G, G, G, G, G, G, G, G, G, G },
				{ I, I, I, I, I, I, I, I, I, I, I, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G },
			}, new Vector2 (5, 6), new List<SerifData>());

		public static MapData Stage08 = new MapData (
			new TileType[,] {
				{ N, N, L7,L7,S1,N, N, D1,L7,L7,I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, D2,D2,R2,N, N, D1,D2,D2,I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, I, I, I, N, N, D1,I, I, I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, I, I, I, N, F, D1,I, I, I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, I, I, I, N, R3,R3,I, I, I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ S3,N, I, I, I, N, N, N, I, I, GL,GL,N, L4,N, N, N, N, N, N, N, N, N, N, N, N },
				{ N, N, I, I, I, N, N, N, I, I, N, N, GL,GL,GL,GL,N, N, N, N, N, N, N, N, N, N },
				{ N, N, I, I, I, N, N, N, I, I, N, N, N, N, N, N, D1,D1,N, N, N, N, N, N, N, N },
				{ L2,N, GL,GL,L1,N, N, N, I, I, N, N, N, N, N, S2,I, I, I, I, N, N, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,I, I, N, N, N, N, N, R3,N, N, D1,D1,N, N, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,I, I, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,R2,R2,N, N, N, N, N, L1,N, N, N, N, G, G, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,R2,R2,GL,GL,G, G, G, G, G, G, G, G, G, G, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,G, G, D3,D3,G, G, G, G, G, G, G, G, G, G, N, N, N, N },
				{ GL,GL,GL,GL,D1,D1,D1,D1,G, G, L5,L5,G, G, G, G, G, G, G, G, G, G, N, N, N, N },
        }, new Vector2 (6, 6), new List<SerifData>());

		public static MapData Stage09 = new MapData (
			new TileType[,] {
				{ N, N, N, N, N, S1,N, L7,N, N, N, N, N, N, N, N, N, N, N, I, N, N, N, N, N, P4},
				{ P3,N, N, N, N, R2,N, D2,N, D3,N, N, GL,GL,GL,N, N, N, N, I, N, N, N, N, GL,GL},
				{ N, N, N, N, N, N, N, N, N, N, N, N, GL,GL,GL,I, I, I, I, L1,I, I, I, I, GL,GL},
				{ N, N, GL,N, N, N, N, N, N, N, N, N, GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL},
				{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
				{ L2,GL,N, N, N, N, N, N, N, N, N, N, N, N, N, N, I, I, I, GL,GL,GL,GL,GL,GL,S3},
				{ D3,GL,N, N, D2,I, N, I, N, N, S2,N, N, N, N, N, N, N, GL,GL,GL,GL,GL,GL,GL,N },
				{ D3,GL,R2,L2,D2,I, N, I, N, N, N, N, N, I, I, I, I, GL,GL,GL,GL,GL,GL,GL,GL,N },
				{ GL,GL,N, N, D2,I, N, I, N, N, R3,N, N, N, N, N, GL,GL,GL,GL,GL,GL,GL,GL,GL,N },
				{ GL,GL,R2,R2,D2,I, N, I, N, N, N, N, N, N, N, N, GL,GL,GL,GL,GL,GL,GL,GL,GL,N },
				{ GL,GL,R2,R2,D2,L1,N, I, N, N, N, N, N, R2,R2,N, GL,GL,GL,GL,GL,GL,GL,GL,GL,N },
				{ GL,GL,N, N, N, D1,D1,I, N, N, L1,N, N, N, N, N, GL,N, N, N, N, N, N, N, I, N },
				{ GL,GL,N, N, N, D1,D1,I, N, N, G, G, N, N, N, N, GL,N, N, N, N, N, N, N, I, N },
				{ GL,GL,P2,N, N, N, N, N, G, G, G, G, N, N, N, N, GL,N, F, N, N, N, N, N, I, N },
				{ GL,GL,G, G, G, D1,D1,G, G, G, G, G, N, N, N, N, GL,R2,R2,R2,R2,R2,R2,D3,D3,D3},

			}, new Vector2 (5, 4), new List<SerifData> () {
				new SerifData(SerifData.unityChanSmileTexture, "드디어 마지막이야!"),
				new SerifData(SerifData.unityChanSmileTexture, "여기만 통과하면 집으로 갈 수 있을거야."),	
			});

        public static MapData DebugStage = new MapData (
            new TileType[,] {
            { N, N, N, N, N, N, N, L3,N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
            { GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N ,N ,G ,GL,GL,GL,G, G, N, N, N, N, N, N, N },
            { GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,N ,N ,G ,GL,S1,R1,D1,L1,N, N, N, N, N, N, N },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, G, G, N, N, N, N, N },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, G, G, N, N },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
            { N, N, N, N, N, N, N, S1,N, N, N, N, N, P1,P1,P1,P1,P1,P1,N, N, N, N, N, N, N },
            { N, N, N, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
            { N, N, N, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N },
            { N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, F, N },
            { N ,N, N, N, N, N, N, L1,N, N, N, N, N, N, N, N, N, N, D1,D1,I, I, I, I, I, I },
            { G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, D1,D1,N ,N, N, N, N, N },
            { G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, D1,D1,N ,N, N, N, N, N },
        }, new Vector2 (5, 14), new List<SerifData>() {
            new SerifData(SerifData.unityChanHappyTexture, "This is debug scene!"),
        });

	}
} 