﻿using System;
using System.Collections.Generic;

namespace UnitychanAdventure.Map
{
    public static class MapManager
    {
        private static int mapIndex; 
        private static List<MapData> mapList;

        public static MapData LastMap = MapDataFactory.Stage09;

        static MapManager()
        {
            mapIndex = 0;
            mapList = new List<MapData> (){
				// Basic
				MapDataFactory.Tutorial00,
				MapDataFactory.Tutorial01,
				MapDataFactory.Tutorial02,
				MapDataFactory.Tutorial03,
				MapDataFactory.Tutorial04,
				// Ice, Glass
				MapDataFactory.Tutorial05,
				MapDataFactory.Tutorial06,
				MapDataFactory.Tutorial07,
				MapDataFactory.Tutorial08,
				MapDataFactory.Tutorial09,
				MapDataFactory.Tutorial10,
				MapDataFactory.Stage01,
				MapDataFactory.Stage02,
				MapDataFactory.Stage03,
				MapDataFactory.Stage04,
				// Piston
				MapDataFactory.Tutorial11,
				MapDataFactory.Tutorial12,
				MapDataFactory.Stage05,
				MapDataFactory.Stage06,
				MapDataFactory.Stage07,
				MapDataFactory.Stage08,
				MapDataFactory.Stage09,
			};
		}

        public static void Reset()
        {
            mapIndex = 0;
        }
        public static MapData GetCurrentMap()
        {
            return mapList[mapIndex];
        }
        public static void MoveToNextMap()
        {
            mapIndex++;
        }
    }
}

