﻿using System;
using Tri.Win;
using UnitychanAdventure.GameLoop;

namespace UnitychanAdventure
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
#if DEBUG
            GameSystem.IsDebug = true;
            GameSystem.DoUnitTest = false;
#else
            GameSystem.DoUnitTest = false;
#endif

            GameSystem.Initialize(1280, 760, "Unitychan Adventure!");
            GameSystem.LoadResources(typeof(Program).Assembly);

            GameSystem.RunGameLoop(new TitleGameLoop());
            GameSystem.Run();
            GameSystem.Dispose();
        }
    }
}
